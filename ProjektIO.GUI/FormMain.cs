﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProjektIO.DatabaseContext;

namespace ProjektIO.GUI
{
	public partial class FormMain : Form
	{
		public FormMain()
		{
			InitializeComponent();
		}

		private void toolStripButtonAddClient_Click(object sender, EventArgs e)
		{
			var dialog = new FormAddClient();
			dialog.MdiParent = this;
			dialog.Show();
		}

		private void toolStripButtonSellKarnet_Click(object sender, EventArgs e)
		{
			var dialog = new FormSellKarnet();
			dialog.MdiParent = this;
			dialog.Show();
		}

		private void toolStripButtonSellTicket_Click(object sender, EventArgs e)
		{
			var dialog = new FormSellTicket();
			dialog.MdiParent = this;
			dialog.Show();
		}
	}
}
