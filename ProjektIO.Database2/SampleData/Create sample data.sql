﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/
/*
INSERT INTO [dbo].[Karnety]
           ([IloscDostZnizekNaMecz]
           ,[IloscDarmowychNaMecz]
           ,[Nazwa]
		   ,[MnoznikZnizki])
     VALUES
           (3, 1, 'Platynowy', 0.5),
		   (2, 0, 'Złoty', 0.5),
		   (1, 0, 'Srebrny', 0.5)
GO
*/