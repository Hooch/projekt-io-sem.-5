﻿CREATE TABLE [dbo].[Karnety]
(
	[Id] BIGINT NOT NULL IDENTITY(1,1), 
    [IloscDostZnizekNaMecz] INT NOT NULL DEFAULT 0, 
    [IloscDarmowychNaMecz] INT NOT NULL DEFAULT 0, 
    [Nazwa] NVARCHAR(50) NOT NULL, 
    [MnoznikZnizki] NUMERIC(4, 2) NOT NULL DEFAULT 1, 
    CONSTRAINT [PK_Karnety] PRIMARY KEY ([Id])
)
