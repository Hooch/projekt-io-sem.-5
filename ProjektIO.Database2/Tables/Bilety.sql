﻿CREATE TABLE [dbo].[Bilety]
(
	[Id] BIGINT NOT NULL IDENTITY(1,1), 
    [CenaBazowa] DECIMAL(18, 4) NOT NULL, 
    [MeczId] BIGINT NOT NULL,
    [Vip] BIT NOT NULL DEFAULT 0, 
    [Sektor] NVARCHAR(1) NOT NULL, 
    [Miejsce] INT NOT NULL, 
    CONSTRAINT [FK_Bilety_Mecze] FOREIGN KEY ([MeczId]) REFERENCES [Mecze]([Id]), 
    CONSTRAINT [PK_Bilety] PRIMARY KEY ([Id])
)
GO
