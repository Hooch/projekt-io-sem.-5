﻿CREATE TABLE [dbo].[Mecze]
(
	[Id] BIGINT NOT NULL  IDENTITY(1,1), 
    [Nazwa] NVARCHAR(50) NOT NULL, 
    [StadionId] BIGINT NOT NULL , 
    [Data] DATETIME2 NOT NULL, 
    CONSTRAINT [FK_Mecze_Stadiony] FOREIGN KEY ([StadionId]) REFERENCES [Stadiony]([Id]), 
    CONSTRAINT [PK_Mecze] PRIMARY KEY ([Id])
)
